### OpenEBench REST API client classes.

This is a set of auxiliary classes to work with OpenEBench REST API.
The OpenEBench endpoint is configured in the META-INF/openebench-repository.cfg 
property.
