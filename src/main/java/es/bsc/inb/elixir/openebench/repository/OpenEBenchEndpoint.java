/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.repository;

import es.bsc.inb.elixir.openebench.model.tools.Tool;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Base64;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;
import javax.json.stream.JsonParser;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchEndpoint {
    
    private static final String DEFAULT_URI_BASE = "https://openebench.bsc.es/monitor";

    public static final String URI_BASE;
    public static final String TOOL_URI_BASE;
    public static final String METRICS_URI_BASE;
    public static final String ALAMBIQUE_URI_BASE;
    
    public static final String TOOL_AVAILABILITY_URI;
    
    static {
        String uri_base;
        try (InputStream in = OpenEBenchEndpoint.class.getClassLoader().getResourceAsStream("META-INF/openebench-repository.cfg")) {
            final Properties p = new Properties();
            p.load(in);
            uri_base = p.getProperty("openebench.uri.base", DEFAULT_URI_BASE);
        } catch (IOException ex) {
            uri_base = DEFAULT_URI_BASE;
        }
        URI_BASE = uri_base;
        TOOL_URI_BASE = uri_base + "/tool/";
        METRICS_URI_BASE = uri_base + "/metrics/";
        ALAMBIQUE_URI_BASE = uri_base + "/alambique/";
        TOOL_AVAILABILITY_URI = uri_base + "/rest/metrics/availability/";
    }

    private final String credentials;
    
    public OpenEBenchEndpoint(String name, String password) {
        String _credentials;
        try {
            final StringBuilder sb = new StringBuilder().append(name).append(':').append(password);
            _credentials = Base64.getEncoder().encodeToString(sb.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            _credentials = "";
        }
        this.credentials = _credentials;
    }
    
    public int put(final Tool tool) throws MalformedURLException, IOException {
        final Jsonb jsonb = JsonbBuilder.create(
                new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE));
        final String json = jsonb.toJson(tool);
        
        final URL url = tool.id.toURL();
        final HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        
        con.setRequestMethod("PUT");
        con.setRequestProperty("Authorization", "Basic " + credentials);
        con.setRequestProperty("Content-type", "application/json");
        try (OutputStream out = con.getOutputStream()) {
            out.write(json.getBytes("UTF-8"));
        }

        return con.getResponseCode();
    }

    public int patch(final Tool tool) throws MalformedURLException, IOException {
        final Jsonb jsonb = JsonbBuilder.create(
                new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE));
        final String json = jsonb.toJson(tool);
        
        final URL url = tool.id.toURL();
        final HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);

        con.setRequestMethod("POST");
        con.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        con.setRequestProperty("Authorization", "Basic " + credentials);
        con.setRequestProperty("Content-type", "application/json");
        try (OutputStream out = con.getOutputStream()) {
            out.write(json.getBytes("UTF-8"));
        }
        
        return con.getResponseCode();
    }
    
    public static Map<String, Tool> get() {
        Map<String, Tool> toolz = new ConcurrentHashMap<>();
        
        final Jsonb jsonb = JsonbBuilder.create(
                new JsonbConfig().withPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE));

        try {
            HttpURLConnection con = (HttpURLConnection)URI.create(TOOL_URI_BASE).toURL().openConnection();
            con.setRequestProperty("Accept", "application/json");
            con.setUseCaches(false);
            con.setDoOutput(true);
            try (InputStream in = con.getInputStream();
                 JsonParser parser = Json.createParser(new BufferedInputStream(in))) {

                if (parser.hasNext() &&
                    parser.next() == JsonParser.Event.START_ARRAY) {

                    final Iterator<JsonValue> iter = parser.getArrayStream().iterator();
                    while(iter.hasNext()) {
                        final JsonValue value = iter.next();
                        if (value.getValueType() == JsonValue.ValueType.OBJECT) {
                            final JsonObject object = value.asJsonObject();
                            final Tool tool = jsonb.fromJson(object.toString(), Tool.class);
                            toolz.put(tool.id.toString(), tool);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ToolsComparator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return toolz;
    }
}
